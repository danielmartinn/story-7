var checkbox = document.querySelector("input[name=theme]");

checkbox.addEventListener("change", function() {
  if (this.checked) {
    trans();
    document.documentElement.setAttribute("data-theme", "dark");
  } else {
    trans();
    document.documentElement.setAttribute("data-theme", "light");
  }
});

let trans = () => {
  document.documentElement.classList.add("transition");
  window.setTimeout(() => {
    document.documentElement.classList.remove("transition");
  }, 1000);
};

$(document).ready(function(){
  $(".accordion_header").click(function(){
    if($(this).hasClass("active")){
      $(this).removeClass("active");
    }
    else{
      $(".accordion_header").removeClass("active");
      $(this).addClass("active");
    }
  });
});
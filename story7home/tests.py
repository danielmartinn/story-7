from django.test import TestCase, Client
from django.http import HttpRequest
from django.urls import resolve

from . import views

class UnitTest(TestCase):
        def test_url_exist(self):
                response= Client().get('/')
                self.assertEqual(response.status_code, 200)
                
        def test_template_exist(self):
                response= Client().get('/')
                self.assertTemplateUsed(response,"index.html")

        def test_file_function(self):
                found = resolve("/")
                self.assertEqual(found.func, views.index)

        def test_content_exist(self):
                self.assertIsNotNone(views.index)

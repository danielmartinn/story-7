function search(){
	var keyword = document.getElementById('search').value
	document.getElementById("result").innerHTML = ""

	$.ajax({
		url: "https://www.googleapis.com/books/v1/volumes?q="+ keyword,
		dataType : "json",

		success: function(data){
			for (i = 0; i < data.items.length; i++){
				result.innerHTML += bookFormat(data.items[i]); 
			}
		},
		type : 'GET'
	});
}

function bookFormat(book, number){
	var {title, authors, imageLinks, publishedDate} = book.volumeInfo;

	return(
		"<tr>" +
				"<td> <img src=" + (imageLinks.smallThumbnail ? imageLinks.smallThumbnail : null) + "></td>" +
				"<td>" + title + "</td>" +
				"<td>" + (authors ? authors : "Unknown") + "</td>" +
				"<td>" + (publishedDate ? publishedDate : "Unknown") + "</td>" +
		"<tr>"
	)
}
document.getElementById('search').addEventListener('keypress', search, false)
document.getElementById('post').addEventListener('click', search, false)
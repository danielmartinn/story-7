from django.test import TestCase, Client, LiveServerTestCase
from django.http import HttpRequest
from django.urls import resolve

from . import views

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time

class UnitTest(TestCase):
        def test_url_exist(self):
                response = Client().get('/books/')
                self.assertEqual(response.status_code, 200)
                
        def test_template_exist(self):
                response = Client().get('/books/')
                self.assertTemplateUsed(response,"books.html")

        def test_file_function(self):
                found = resolve("/books/")
                self.assertEqual(found.func, views.books)

        def test_content_exist(self):
                self.assertIsNotNone(views.books)

        def test_correct_static(self):
                response = Client().get('/books/')
                self.assertContains(response, 'static/javascript/books')
                self.assertContains(response, 'static/css/books')

# class FunctionalTest(LiveServerTestCase):
#         def setUp(self):
#                 chrome_options = Options()
#                 chrome_options.add_argument('--dns-prefetch-disable')
#                 chrome_options.add_argument('--no-sandbox')
#                 chrome_options.add_argument('--headless')
#                 chrome_options.add_argument('disable-gpu')
#                 self.browser  = webdriver.Chrome(executable_path='./chromedriver.exe', chrome_options=chrome_options)
#                 super(FunctionalTest, self).setUp()

#         def tearDown(self):
#                 self.browser.quit()
#                 super(FunctionalTest, self).tearDown()

#         def test_success_append(self):
#                 self.browser(self.live_server_url)
#                 time.sleep(2)
#                 search = self.browser.find_element_by_id('search')
#                 time.sleep(2)

#                 post = self.browser.find_element_by_id('post')

#                 search.send_keys('bumi')
#                 post.click()
#                 time.sleep(2)

#                 result = self.browser.find_element_by_id('item')

#                 status = 0
#                 if (len(result) != 0):
#                         status = 200
#                 else:
#                         status = 404

#                 self.assertEqual(status, 200)